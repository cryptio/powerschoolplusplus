/*
* PowerSchoolPlusPlus

* This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

/**
 * @file powerschoolplusplus.cpp
 *
 * @brief Defines a C++ PowerSchool SOAP client
 *
 * @author Encryptio
 * Contact: cryptio@pm.me
 */

#include "include/powerschoolplusplus.h"
#include "gsoap/PublicPortalServiceJSONSoap11Binding.nsmap"
#include "gsoap/soapPublicPortalServiceJSONSoap11BindingProxy.h"
#include "gsoap/soapH.h"
#include "gsoap/plugin/httpda.h"
#include <algorithm>

PowerSchoolPlusPlus::PowerSchoolPlusPlus(std::string url, std::string api_username, std::string api_password)
        : url_(std::move(url) + "/pearson-rest/services/PublicPortalServiceJSON?wsdl"),
          api_username_(std::move(api_username)), api_password_(std::move(api_password)),
          soap_(soap_new()), service_(new PublicPortalServiceJSONSoap11BindingProxy), info_(new http_da_info) {
    setup();
}

void PowerSchoolPlusPlus::setup() {
    soap_register_plugin(service_->soap, http_da);
    service_->soap_endpoint = url_.c_str();
}

std::string PowerSchoolPlusPlus::get_url() const {
    return url_;
}

std::string PowerSchoolPlusPlus::get_api_username() const {
    return api_username_;
}

std::string PowerSchoolPlusPlus::get_api_password() const {
    return api_password_;
}

std::pair<ns5__UserSessionVO, bool>
PowerSchoolPlusPlus::loginToPublicPortal(const std::string &username, const std::string &password) {
    _ns4__loginToPublicPortal login;
    _ns4__loginToPublicPortalResponse login_response;
    login.soap = soap_.get();
    login.username = new std::string(username);
    login.password = new std::string(password);
    if (service_->loginToPublicPortal(&login, login_response)) {
        if (service_->soap->error == 401) {
            // We need authorization! Retrieve the HTTP DA realm sent by the server, give our credentials, and retry
            // std::cout << "[*] 401 Unauthorized received, re-trying with HTTP Digest Authentication...\n";
            const char *realm = service_->soap->authrealm;
            http_da_save(service_->soap, info_.get(), realm, api_username_.c_str(), api_password_.c_str());

            if (service_->loginToPublicPortal(url_.c_str(), nullptr, &login, login_response) == SOAP_OK) {
                if (!login_response.return_->userSessionVO) {
                    if (!login_response.return_->messageVOs.empty()) {
                        for (const auto &message_vo: login_response.return_->messageVOs) {
                            message_vos_.push_back(new ns5__MessageVO(*message_vo));
                        }
                    }
                    return std::pair<ns5__UserSessionVO, bool>(ns5__UserSessionVO(), false);
                }
                return std::pair<ns5__UserSessionVO, bool>(*login_response.return_->userSessionVO, true);
            }
        }

        if (!login_response.return_->userSessionVO) {
            if (!login_response.return_->messageVOs.empty()) {
                for (const auto &message_vo: login_response.return_->messageVOs) {
                    message_vos_.push_back(new ns5__MessageVO(*message_vo));
                }
            }

            return std::pair<ns5__UserSessionVO, bool>(ns5__UserSessionVO(), false);
        }
        return std::pair<ns5__UserSessionVO, bool>(*login_response.return_->userSessionVO, true);
    }

    return std::pair<ns5__UserSessionVO, bool>(ns5__UserSessionVO(), false);
}

std::vector<ns5__MessageVO> PowerSchoolPlusPlus::get_message_vos() const {
    std::vector<ns5__MessageVO> message_vos_to_return;
    if (!message_vos_.empty()) {
        for (const auto &message_vo: message_vos_) {
            message_vos_to_return.push_back(*message_vo);
        }
    }

    return message_vos_to_return;
}

std::pair<std::vector<ns5__StudentDataVO>, bool>
PowerSchoolPlusPlus::getStudentData(std::vector<int64_t> student_ids, ns5__UserSessionVO *user_session) {
    _ns4__getStudentData data;
    _ns4__getStudentDataResponse data_response;
    data.soap = soap_.get();
    data.studentIDs = std::move(student_ids);
    data.userSessionVO = user_session;
    ns5__QueryIncludeListVO query_include_list;
    query_include_list.includes = {
            1
    };
    data.qil = &query_include_list;
    std::vector<ns5__StudentDataVO> v_student_data_vos;
    if (service_->getStudentData(&data, data_response)) {
        if (service_->soap->error == 401) {
            // std::cout << "[*] 401 Unauthorized received, re-trying with HTTP Digest Authentication...\n";
            const char *realm = service_->soap->authrealm;
            http_da_save(service_->soap, info_.get(), realm, api_username_.c_str(), api_password_.c_str());

            if (service_->getStudentData(&data, data_response) == SOAP_OK) {
                if (!data_response.return_) {
                    return std::pair<std::vector<ns5__StudentDataVO>, bool>(std::vector<ns5__StudentDataVO>(), false);
                }

                for (const auto &student_vo: data_response.return_->studentDataVOs) {
                    v_student_data_vos.push_back(*student_vo);
                }
                return std::pair<std::vector<ns5__StudentDataVO>, bool>(v_student_data_vos, true);
            }
        }

        if (!data_response.return_) {
            return std::pair<std::vector<ns5__StudentDataVO>, bool>(std::vector<ns5__StudentDataVO>(), false);
        }
        for (const auto &element: data_response.return_->studentDataVOs) {
            v_student_data_vos.push_back(*element);
        }

        return std::pair<std::vector<ns5__StudentDataVO>, bool>(v_student_data_vos, true);
    }

    return std::pair<std::vector<ns5__StudentDataVO>, bool>(std::vector<ns5__StudentDataVO>(), false);
}


PowerSchoolPlusPlus::~PowerSchoolPlusPlus() {
    ;
}