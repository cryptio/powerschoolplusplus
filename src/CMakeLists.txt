cmake_minimum_required(VERSION 3.13)
project(soap)

set(CMAKE_CXX_STANDARD 14)

add_executable(soap main.cpp include/powerschoolplusplus.h powerschoolplusplus.cpp ${PROJECT_SOURCE_DIR}/gsoap/soapC.cpp ${PROJECT_SOURCE_DIR}/gsoap/stdsoap2.cpp ${PROJECT_SOURCE_DIR}/gsoap/soapPublicPortalServiceJSONSoap11BindingProxy.cpp ./gsoap/plugin/httpda.c ${PROJECT_SOURCE_DIR}/gsoap/plugin/smdevp.c ${PROJECT_SOURCE_DIR}/gsoap/plugin/threads.c)
add_compile_definitions(WITH_OPENSSL DEBUG)

target_link_libraries(soap -lgsoap++ -lssl -lcrypt -lcrypto)