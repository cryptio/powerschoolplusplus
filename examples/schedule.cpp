/**
 * wsdl2h PublicPortalServiceJSON
 * soapcpp2 -j -C -Iimport PublicPortalServiceJSOH.h
 */

#include <iostream>
#include "include/powerschoolplusplus.h"
#include "gsoap/soapH.h"
#include <sstream>
#include <string>
#include <ostream>
#include <ctime>
#include <algorithm>
#include <termios.h>
#include <unistd.h>

/*
 * @brief Represents an entry in the student's schedule
 */
struct ScheduleEntry {
    ScheduleEntry()
            : start(0), stop(0) {
        ;
    }

    std::string course_title;
    std::string course_code;
    std::string teacher_first_name;
    std::string teacher_last_name;
    std::string room_name;
    std::time_t start;
    std::time_t stop;
};

/**
 * @brief Represents a student's monthly schedule
 */
struct Schedule {
    std::vector<ScheduleEntry> schedule_entries;
};


/**
 * @brief Allows for the printing of a pointer without the worry of a null pointer exception
 *
 * @tparam T
 * @param t
 * @return
 */
template<class T>
T safe_ptr_access(const T *t) {
    if (t) {
        return *t;
    }
    T default_t;
    return default_t;
}

/**
 * @brief Read the portal credentials from stdin
 *
 * @return
 */
std::pair<std::string, std::string> read_portal_credentials() {
    std::cout << "Username:\n>";
    std::string username;
    std::getline(std::cin, username);

    std::cout << "Password:\n>";
    std::string password;
    std::getline(std::cin, password);

    return std::pair<std::string, std::string>(username, password);
}

void sort_startstopdates(std::vector<ns5__StartStopDateVO *> &startstopdates) {
    std::sort(startstopdates.begin(), startstopdates.end(),
              [](ns5__StartStopDateVO *lhs, ns5__StartStopDateVO *rhs) -> bool {
                  return (*lhs->start < *rhs->start);
              });
}

void sort_schedule(Schedule &schedule) {
    std::sort(schedule.schedule_entries.begin(), schedule.schedule_entries.end(),
              [](const ScheduleEntry &lhs, const ScheduleEntry &rhs) -> bool {
                  return (lhs.start < rhs.start);
              });
}

int main() {
    PowerSchoolPlusPlus powerSchoolPlusPlus("https://powerschoolpublicportallink.com");
    std::pair<std::string, std::string> username_and_password = read_portal_credentials();
    auto login_result = powerSchoolPlusPlus.loginToPublicPortal(username_and_password.first,
                                                                username_and_password.second);
    // Try to login
    if (!login_result.second) {
        std::cout << "[-] Login failed";
        // Print the error information if there is any
        const auto message_vos = powerSchoolPlusPlus.get_message_vos();
        for (const auto &message_vo: message_vos) {
            if (message_vo.msgCode) {
                /**
                 * Some of these pointers may be null. If we try dereferencing a null pointer the program will crash, so we must ensure that they hold a memory address first
                 */
                std::cout << ":\n\tTitle: " << safe_ptr_access(message_vo.title) << '\n';
                std::cout << "\tDescription: " << safe_ptr_access(message_vo.description) << '\n';
                std::cout << "\tMessage code: " << *message_vo.msgCode;
            }
        }
        std::cout << '\n';
        return EXIT_FAILURE;
    }
    // Login success, print the received data...
    std::cout << "[+] Login success\n";

    const auto data_result = powerSchoolPlusPlus.getStudentData(
            std::vector<int64_t>(login_result.first.studentIDs.begin(), login_result.first.studentIDs.end()),
            &login_result.first);
    if (!data_result.second) {
        std::cout << "[-] getStudentData() error!\n";
        return EXIT_FAILURE;
    }
    std::cout << "[+] getStudentData() success!\n";
    for (const auto &student_vo: data_result.first) {
        //std::cout << "[*] Student: " << safe_ptr_access(student_vo.student->lastName) << ", "
        //<< safe_ptr_access(student_vo.student->firstName) << '\n';
        std::cout << "[*] Student: " << safe_ptr_access(student_vo.student->lastName) << ", "
                  << safe_ptr_access(student_vo.student->firstName) << '\n';

        Schedule schedule;
        for (const auto &section: student_vo.sections) {
            sort_startstopdates(section->startStopDates);
            std::time_t now_time_t = std::time(nullptr);
            tm now = *std::localtime(&now_time_t);
            // The startStopDates is covered on a monthly basis
            for (const auto &date: section->startStopDates) {
                ScheduleEntry entry;
                entry.course_title = *section->schoolCourseTitle;
                entry.course_code = *section->courseCode;
                const auto it = std::find_if(student_vo.teachers.begin(), student_vo.teachers.end(),
                                             [&section](const ns5__TeacherVO *teacher) -> bool {
                                                 return (*section->teacherID == *teacher->id);
                                             });
                if (it == student_vo.teachers.end()) {
                    std::cout << "[-] Unable to find corresponding TeacherVO for section "
                              << safe_ptr_access(section->courseCode) << '\n';
                } else {
                    entry.teacher_first_name = *it.operator*()->firstName;
                    entry.teacher_last_name = *it.operator*()->lastName;
                    entry.room_name = *section->roomName;
                    entry.start = *date->start;
                    entry.stop = *date->stop;
                    schedule.schedule_entries.push_back(entry);
                }
            }
        }
        if (schedule.schedule_entries.empty()) {
            std::cout << "[-] Unable to get today's schedule for " << safe_ptr_access(student_vo.student->lastName)
                      << ", "
                      << safe_ptr_access(student_vo.student->firstName) << '\n';
        } else {
            sort_schedule(schedule);

            // Print out the schedule in the correct order
            for (const auto &entry: schedule.schedule_entries) {
                std::cout << entry.course_title << '\n';
                std::cout << entry.course_code << '\n';
//                std::cout << entry.teacher_last_name << ", " << entry.teacher_first_name << '\n';
                std::cout << entry.teacher_last_name << ", " << entry.teacher_first_name << '\n';
                std::cout << "Room: " << entry.room_name << '\n';
                std::cout << std::asctime(std::localtime(&entry.start));
                std::cout << " - ";
                std::cout << std::asctime(std::localtime(&entry.stop)) << '\n';
                std::cout << '\n';
            }
        }
    }
}