/*
* PowerSchoolPlusPlus

* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

/**
 * @file powerschoolplusplus.h
 *
 * @brief Defines a C++ PowerSchool SOAP client
 *
 * @author Encryptio
 * Contact: cryptio@pm.me
 */

#ifndef POWERSCHOOLPLUSPLUS_POWERSCHOOLPLUSPLUS_H
#define POWERSCHOOLPLUSPLUS_POWERSCHOOLPLUSPLUS_H

#include <string>
#include <memory>
#include <vector>

// Forward declare these structs, otherwise we will violate the One Definition Rule (ODR) and get redefinition errors
struct soap;

class PublicPortalServiceJSONSoap11BindingProxy;

struct http_da_info;
struct _ns4__loginToPublicPortalResponse;
struct ns5__UserSessionVO;
struct ns5__StudentDataVO;
struct ns5__TeacherVO;
struct ns5__MessageVO;

class PowerSchoolPlusPlus {
public:

    explicit PowerSchoolPlusPlus(std::string url, std::string api_username = "pearson",
                                 std::string api_password = "m0bApP5");

    /**
     * @brief Initialize the SOAP client
     */
    void setup();

    /**
     * @brief Get the URL being used
     *
     * @return The URL in use
     */
    std::string get_url() const;

    /**
     * @brief Retrieve the API username for HTTP Digest Authentication
     *
     * @return Current API username
     */
    std::string get_api_username() const;

    /**
     * @brief Retrieve the API password for HTTP Digest Authentication
     *
     * @return Current API password
     */
    std::string get_api_password() const;

    /**
     * @brief Attempt to login to the public portal
     *
     * @param username The username to login with
     * @param password The password to login with
     * @return The UserSessionVO object and a bool indicating whether or not it succeeded
     */
    std::pair<ns5__UserSessionVO, bool> loginToPublicPortal(const std::string &username, const std::string &password);

    /**
     * @brief Get all of the MessageVOs
     *
     * @return Vector of MessageVOs
     */
    std::vector<ns5__MessageVO> get_message_vos() const;

    /**
     * @brief Get the student data identified by their student ID(s) and user session
     *
     * @param student_ids The list of their student IDs
     * @param user_session A valid user session that has been returned from loginToPublicPortal
     * @return A vector of StudentDataVOs containing as much student data as possible
     */
    std::pair<std::vector<ns5__StudentDataVO>, bool>
    getStudentData(std::vector<int64_t> student_ids, ns5__UserSessionVO *user_session);

    // A destructor needs to be declared in order to use unique_ptr with incomplete types
    ~PowerSchoolPlusPlus();

private:
    std::string url_;
    std::string api_username_;
    std::string api_password_;
    /*
     * Since we are using incomplete types to declare our members, we must use pointers.
     * In order to safely use pointers, smart pointers are used to manage the resources
     */
    std::unique_ptr<struct soap> soap_;
    std::unique_ptr<PublicPortalServiceJSONSoap11BindingProxy> service_;
    std::unique_ptr<struct http_da_info> info_;
    std::vector<ns5__MessageVO *> message_vos_;
};


#endif //POWERSCHOOLPLUSPLUS_POWERSCHOOLPLUSPLUS_H
